### Varifr�n kommer f�rem�len p� kluemarket?

Kluemarket bevakar Blocket och m�ngder av Facebook grupper d�r folk s�ljer egna
egodelar. Nya annonser som matchar v�ra kriterier p� kvalit� l�ggs upp p� kluemarket
timmvis.

Vi plockar bort alla s�lda f�rem�l regelbundet, och d�rmed �r annonser p�
kluemarket garranterat f�rska och aktuella.

### Hur k�per jag ett f�rem�l jag gillar?

Tryck p� k�p knappen under f�rem�lets beskrivning.

Om f�rem�let kommer fr�n en Blocket annons kommer du att skickas till annonsen
och du f�r kontakta s�ljaren d�r, som vanligt med Blocket.

Om f�rem�let s�ljs av en privat person p� Facebook blir du skickad till
personens s�ljpost i motsvarande Facebook grupp d�r du kan chatta direkt med
s�ljaren. Om du inte �r medlem i gruppen och den inte �r �pen f�r allm�nheten
kommer du beh�va ans�ka om medlemskap f�rst f�r att n� till s�ljaren.

### Vad betyder st�mplarna med hj�rtat och kronan?

V�ra kunniga experter g�r regelbundet igenom annonserna som vi h�mtar fr�n
Facebook grupper och Blocket och v�ljer ut de som �r extra intressanta. Det
handlar oftast om att f�rem�let har bevisat v�rde, �r �kta, en fynd eller av
s�rskilt sp�nnande natur. Dessa f�rem�l st�mplas med ett hj�rta och visas
bland de f�rsta n�r du s�ker p� kluemarket.

Annonser st�mplade med en r�d krona betyder att f�rem�let har granskats och
beskrivits av Klues egna experter, vilket medger samma garantier p� kvalit�,
�kthet och pris som du skulle f� p� klassiska auktionshus, fast till betydligt
l�gre kostnad f�r b�de s�ljaren och k�paren. L�s mer om [hur du kan sj�lv
s�lja](https://kluemarket.com/sv/sell) p� kluemarket.

### Hur s�ljer jag n�got p� kluemarket?

Det �r enkelt:

1. Ta kontakt med oss via [Facebook Messenger]() eller v�r [iPhone app]() och
best�ll en certifikat.

2. Klues experter v�rderar och beskriver ditt f�rem�l och du f�r en l�nk till
en websida som beskriver f�rem�let.

3. Dela l�nken till denna websida i en av de Facebook grupper som bevakas av
kluemarket, eller klistra in den i din Blocket annons, och ditt f�rem�l dyker
upp automatiskt p� kluemarket!

4. K�parna tar kontakt med dig direkt via Blocket eller Facebook. Du f�rhandlar
sj�lv med dem om pris och h�mtning. Inga mellanavgifter eller l�nga v�ntetider!


Mer information om hur du s�ljer p� kluemarket [hittar du
h�r](https://kluemarket.com/sv/sell).
